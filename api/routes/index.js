const { TwitterApi } = require('twitter-api-v2');

const axios = require("axios");
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/getUsers', async function(req, res, next) {
  let nameUser = req.query.name;
  
  const twitterClient = new TwitterApi('AAAAAAAAAAAAAAAAAAAAAMBOYwEAAAAAQre30BTwpOtMYoUAduB3TtnlnP8%3DbARMNpkwusTjP4W8QSGbDeKHRNB3f1cRkP1qZs7z7yteUeqeYd');
  const roClient = twitterClient.readOnly;
  const user = await roClient.v2.usersByUsernames(nameUser, { 'user.fields': ['profile_image_url'] });

  res.send(user);
});

router.get('/getTweets', async function(req, res, next) {
  let nameUser = req.query.name;
  
  const twitterClient = new TwitterApi('AAAAAAAAAAAAAAAAAAAAAMBOYwEAAAAAQre30BTwpOtMYoUAduB3TtnlnP8%3DbARMNpkwusTjP4W8QSGbDeKHRNB3f1cRkP1qZs7z7yteUeqeYd');
  const roClient = twitterClient.readOnly;
  const user = await roClient.v2.search('from:'+nameUser);
  
  res.send(user);
});

router.post('/getTweetsByID', async function(req, res, next) {
  //test fonctionnel de requette de l'api 
  let id = req.body.id;

  let config = {
    headers: {
      'Authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAAMBOYwEAAAAAQre30BTwpOtMYoUAduB3TtnlnP8%3DbARMNpkwusTjP4W8QSGbDeKHRNB3f1cRkP1qZs7z7yteUeqeYd',
    }
  }
  axios.get('https://api.twitter.com/2/users/'+id+'/tweets', config)
  .then(function (response) {
    res.send(response.data)
  });
});

router.post('/getTweetsLastWeek', async function(req, res, next) {
  let nameUser = req.body.nameUser;


  let config = {
    headers: {
      'Authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAAMBOYwEAAAAAQre30BTwpOtMYoUAduB3TtnlnP8%3DbARMNpkwusTjP4W8QSGbDeKHRNB3f1cRkP1qZs7z7yteUeqeYd',
    }
  }
  axios.get('https://api.twitter.com/2/tweets/counts/recent?query=from:'+nameUser+'&granularity=day', config)
  .then(function (response) {
    res.send(response.data)
  });
});




module.exports = router;
