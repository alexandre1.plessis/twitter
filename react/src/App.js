import React, { useState, useEffect } from "react";
import './App.css';
import axios from 'axios';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

function App() {

  //stats : 
  const [infosCandidats, setInfosCandidats] = useState(undefined);
  const [candidatSelected, setCandidatSelected] = useState(undefined);
  const [tweets, setTweets] = useState(undefined);
  const [dataNbTweets, setDataNbTweets] = useState(undefined);
  const [dataGraph, setDataGraph] = useState(undefined);

  const candidats = [
    {name:'Nathalie Arthaud', userName:'n_arthaud'},
    {name:'Nicolas Dupont-Aignan', userName:'dupontaignan'},
    {name:'Anne Hidalgo', userName:'Anne_Hidalgo'},
    {name:'Yannick Jadot', userName:'yjadot'},
    {name:'Jean Lassalle', userName:'jeanlassalle'},
    {name:'Marine Le Pen', userName:'MLP_officiel'},
    {name:'Emmanuel Macron', userName:'EmmanuelMacron'},
    {name:'Jean-Luc Mélenchon', userName:'JLMelenchon'},
    {name:'Valérie Pécresse', userName:'vpecresse'},
    {name:'Philippe Poutou', userName:'PhilippePoutou'},
    {name:'Fabien Roussel', userName:'Fabien_Roussel'}, 
    {name:'Eric Zemmour', userName:'ZemmourEric'}
  ];
  const daysWeek = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

  //conter nb tweets de la semaine
  function countTweets (tab) {
    let nb = 0;
    tab.forEach(element => {
        nb += element.tweet_count;
    });
    return nb;
  }

  // graph
  function daysgraph () {
    let week = [];
    let currentDay = new Date().getUTCDay();
    for(let i = 0; i < 7; i++){
      if (currentDay > -1) {
        week.unshift(daysWeek[currentDay]);
      } else {
        week.unshift(daysWeek[-currentDay%7]);
      }
      currentDay--;
    }
    return week;
  }

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip
  );

  const options = {
    responsive: true,
    plugins: {
      title: {
        display: true,
        text: 'Nombre de tweets ces 7 derniers jours',
      },
    },
  };


  const labels = daysgraph();
  var data = undefined;
  if (dataGraph !== undefined) {
    const tabData = dataGraph.map(data =>data.tweet_count );

    data = {
      labels,
      datasets: [
        {
          label: 'Dataset 1',
          data: labels.map((e, index) =>  tabData[index]),
          borderColor: 'rgb(255, 99, 132)',
          backgroundColor: 'rgba(255, 99, 132, 0.5)',
        },
      ],
    };
  }

  useEffect(() => {
    if (infosCandidats === undefined) {
      //récupération des données de tout les candidats
      axios.all(candidats.map((candidat) => axios.get('http://localhost:3001/getUsers?name='+candidat.userName))).then(
        (data) => {
          setInfosCandidats(data);
        },
      );
    } else {
      //initialisation des données du graph
      if (dataGraph === undefined && candidatSelected) {
        axios.post('http://localhost:3001/getTweetsLastWeek', {nameUser: candidats[candidatSelected].userName})
        .then(function (response) {
          setDataNbTweets(countTweets(response.data.data));
          setDataGraph(response.data.data);
        });
      }
    }
  });

  function handleChangeSelectedCandidat(event, candidat) {  
    setCandidatSelected(candidat);  
    axios.post('http://localhost:3001/getTweetsByID', {id: infosCandidats[candidat].data.data[0].id})
    .then(function (response) {
      console.log(response);
      setTweets(response.data.data);
    });
    axios.post('http://localhost:3001/getTweetsLastWeek', {nameUser: candidats[candidat].userName})
    .then(function (response) {
      setDataNbTweets(countTweets(response.data.data));
      setDataGraph(response.data.data);
    });
  }

  
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <main className="App-main">

        {/* Candidats */}
        <div className="choixCandidat">
          <p>Choisissez un candidat:</p>
          <div className="candidats">
            {infosCandidats !== undefined && infosCandidats.map((candidat, index) => 
              <div key={index} username={candidat.data.data[0].userName} className="candidat" onClick={(event) => handleChangeSelectedCandidat(event, index)} >
                <img src={candidat.data.data[0].profile_image_url} title="" alt="" />
                <span>{candidat.data.data[0].name}</span>
                <span>@{candidat.data.data[0].username}</span>
              </div>
            )}
          </div>
        </div>

        {/* Tweets */}
        <div className="tweets">
          <p>Tweets:</p>
          {tweets !== undefined && tweets.map((twitt, index) => <p key={index}>{twitt.text}</p>)}
        </div>

        {/* Graph */}
        <div className="graphe">
          <p>Statistiques:</p>
          {dataGraph !== undefined && <Line options={options} data={data} />}
          {dataNbTweets !== undefined &&<p>Nombre de tweets envoyé ces sept derniers jours : {dataNbTweets}</p>}
        </div>

      </main>
    </div>
  );
}

export default App;