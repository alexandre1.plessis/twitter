import logo from '../twitter.png';
import React from "react";
import '../App.css';
import TextField from "@material-ui/core/TextField";

function Test() {
  console.log(logo);
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Une application avec l'api Twitter.
        </p>
        <img src={logo} className="App-logo" alt="logo" />
        <br/>
        <TextField
          id="outlined-basic"
          label="Entrez un pseudo"
          variant="outlined"
        />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Lancer la recherche
        </a>
      </header>
    </div>
  );
}
export default Test;
